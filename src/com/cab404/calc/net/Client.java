package com.cab404.calc.net;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

/**
 * @author cab404
 */
public class Client {
    private Socket socket;
    private BufferedReader reader;
    private PrintWriter writer;

    public Client(String address, int port)
    throws IOException {
        socket = new Socket(address, port);
        writer = new PrintWriter(socket.getOutputStream(), true);
        reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
    }

    public double calculate(String expression)
    throws IOException {
        writer.println("CALC " + expression);
        return Double.parseDouble(reader.readLine());
    }

    public void close()
    throws IOException {
        writer.println("END");
        socket.close();
    }

}

package com.cab404.calc.net;

import com.cab404.calc.Calculation;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * @author cab404
 */
public class Server implements Runnable {
    protected boolean isRunning = true;


    class ListenerStream implements Runnable {
        PrintWriter writer;
        BufferedReader reader;
        Socket soc;
        Calculation calc;

        ListenerStream(Socket socket)
        throws IOException {
            soc = socket;
            calc = new Calculation();
            writer = new PrintWriter(socket.getOutputStream(), true);
            reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        }
        @Override public void run() {
            while (isRunning && !soc.isClosed()) {
                try {
                    String read = reader.readLine();
                    if (read == null || read.startsWith("END")) {
                        soc.close();
                        break;
                    }
                    if (read.startsWith("CALC"))
                        try {
                            calc.prepareExpression(read.split(" ", 2)[1]);
                            if (!read.startsWith("CALC_NO_ANSWER")) {
                                writer.println(calc.calculate());
                            }
                        } catch (Throwable t) {
                            soc.close();
                            break;
                        }

                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            System.out.println("Клиент отключен - " + soc.getInetAddress());
        }
    }

    ServerSocket soc;
    public Server(int port)
    throws IOException {
        soc = new ServerSocket(port);
    }
    @Override public void run() {
        while (isRunning) {
            try {
                Socket client = soc.accept();
                System.out.println("Новый клиент - " + client.getInetAddress());
                Thread thread = new Thread(new ListenerStream(client));
                thread.setDaemon(true);
                thread.start();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}

package com.cab404.calc;

import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Map;

/**
 * @author cab404
 */
class ArgumentNode extends Node {
    private Argument value;

    private static Map<Class<? extends Argument>, String> expressions;
    static {
        expressions = new HashMap<>();
        expressions.put(NumberArgument.class, "((\\+|\\-|)[0-9\\.]+)");
        expressions.put(StringArgument.class, "(\"[A-z0-9_]*?\")");
        expressions.put(VariableArgument.class, "(\\:[A-z0-9_]+?:)");
    }

    static String getStatementExpression() {
        StringBuilder ret = new StringBuilder();

        for (String exp : expressions.values())
            ret.append(exp).append("|");

        ret.deleteCharAt(ret.length() - 1);

        return ret.toString();
    }

    protected void init() {
        for (Map.Entry<Class<? extends Argument>, String> entry : expressions.entrySet()) {
            if (getText().matches(entry.getValue())) try {

                value = entry.getKey().getConstructor(String.class).newInstance(getText());
                break;

            } catch (InstantiationException | IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
                throw new RuntimeException(e);
            }
        }
    }

    Argument getArgument() {
        return value;
    }
    void setArgument(Argument value) {
        this.value = value;
    }

    @Override public String toString() {
        return value + "";
    }
}

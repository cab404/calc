package com.cab404.calc;

/**
 * @author cab404
 */
class StringArgument extends Argument {
    private String value;

    public StringArgument(String value) {
        super(value);
        this.value = value.substring(1, value.length() - 1);
    }

    @Override public Type getType() {
        return Type.STRING;
    }

    @Override public void setValue(Object value) {
        throw new UnsupportedOperationException();
    }

    @Override public Object getValue() {
        return value;
    }

    @Override public boolean isConstant() {
        return false;
    }

    @Override public String toString() {
        return value;
    }
}

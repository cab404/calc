package com.cab404.calc;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author cab404
 */
public class Calculation {
    static {
        BasicCommands.add();
    }

    List<Node> nodes = new LinkedList<>();
    ArrayList<VariableArgument> variables = new ArrayList<>();

    public void prepareExpression(String expression) {

        Pattern pattern = Pattern.compile(Node.getStatementExpression());
        Matcher matcher = pattern.matcher(expression);

        nodes.clear();

        while (matcher.find()) {
            String part = expression.substring(matcher.start(), matcher.end());
            nodes.add(Node.create(part));
        }

        insertVariables(nodes);
        wrapBracketNodes(nodes);
    }

    /**
     * Переменные!
     */
    private void insertVariables(List<Node> nodes) {
        for (Node node : nodes) {
            if (node instanceof ArgumentNode) {
                if (((ArgumentNode) node).getArgument() instanceof VariableArgument) {
                    if (variables.contains(((ArgumentNode) node).getArgument())) {
                        ((ArgumentNode) node).setArgument(variables.get(variables.indexOf(((ArgumentNode) node).getArgument())));
                    } else {
                        variables.add((VariableArgument) ((ArgumentNode) node).getArgument());
                    }
                }
            }
        }
    }

    private static StringBuffer print(List<Node> nodes, String offset, StringBuffer buf) {
        int i = 0;
        for (Node node : nodes) {
            if (node instanceof BracketsNode)
                print(((BracketsNode) node).nodes, offset + i + ".", buf);
            else
                buf
                        .append(offset)
                        .append(i)
                        .append(". ")
                        .append(node.getClass().getSimpleName())
                        .append(": ")
                        .append(node)
                        .append('\n');
            i++;
        }
        return buf;
    }

    @Override public String toString() {
        return print(nodes, "", new StringBuffer()).toString();
    }
    private void wrapBracketNodes(List<Node> nodes) {
        int level = 0;

        BracketsNode tmp = null;

        // Сворачиваем все скобки
        for (int i = 0; i < nodes.size(); ) {

            if (nodes.get(i) instanceof CloseBracketNode) level--;

            if (tmp == null) {
                if (nodes.get(i) instanceof BracketsNode) {
                    level++;
                    tmp = (BracketsNode) nodes.get(i);
                }
            } else {
                if (nodes.get(i) instanceof BracketsNode) level++;
                if (level > 0) {
                    tmp.nodes.add(nodes.remove(i));
                    continue;
                }
                if (level == 0) {
                    wrapBracketNodes(tmp.nodes);
                    tmp = null;
                }
            }

            if (level < 0) throw new RuntimeException("Скобки расставлены неправильно!");
            i++;
        }

        if (level != 0) throw new RuntimeException("Скобки расставлены неправильно!");

        // Убирем закрывающие скобки

        for (int i = 0; i < nodes.size(); ) {
            if (nodes.get(i) instanceof CloseBracketNode)
                nodes.remove(i);
            else i++;
        }

    }

    public Argument calculate() {
        return calculate(nodes);
    }

    private void calculateFunction(List<Node> nodes, int i) {
        ListIterator<Node> iterator;
        iterator = nodes.listIterator(i);

        Node node = nodes.get(i);

        if (node instanceof FunctionNode) {
            Argument[] prev_val = new Argument[0], next_val = new Argument[0];

            // Разбираемся с префиксными аргументами
            if (i - 1 != -1) {
                if (nodes.get(i - 1) instanceof ArgumentNode) {
                    ArgumentNode prev = (ArgumentNode) nodes.get(i - 1);
                    prev_val = new Argument[1];
                    prev_val[0] = prev.getArgument();
                }
                if (nodes.get(i - 1) instanceof BracketsNode) {
                    iterator.previous();

                    ArgumentNode ans = new ArgumentNode();
                    ans.setArgument(calculate(((BracketsNode) nodes.get(i - 1)).nodes));

                    iterator.set(ans);
                    iterator.next();

                    prev_val = new Argument[1];
                    prev_val[0] = ans.getArgument();
                }
            }


            // Разбираемся с постфиксными аргументами
            if (i + 1 != nodes.size()) {
                // Просто число
                if (nodes.get(i + 1) instanceof ArgumentNode) {
                    ArgumentNode next = (ArgumentNode) nodes.get(i + 1);
                    next_val = new Argument[1];
                    next_val[0] = next.getArgument();
                }
                // Несколько (или одно) чисел/выражений в скобке.
                if (nodes.get(i + 1) instanceof BracketsNode) {
                    BracketsNode brackets = (BracketsNode) nodes.get(i + 1);

                    ArrayList<Node> temporary = new ArrayList<>();
                    ArrayList<Argument> calculated = new ArrayList<>();

                    for (int j = 0; j < brackets.nodes.size(); j++) {
                        Node tmp_node = brackets.nodes.get(j);

                        if (tmp_node instanceof DelimeterNode) {
                            i++;
                            calculated.add(calculate(temporary));
                            temporary.clear();
                        } else {
                            temporary.add(tmp_node);
                        }

                    }

                    calculated.add(calculate(temporary));
                    temporary.clear();

                    next_val = new Argument[calculated.size()];

                    for (int j = 0; j < calculated.size(); j++)
                        next_val[j] = calculated.get(j);

                }

            }

            // Удаляем все использованные параметры
            if (prev_val.length != 0) {
//            print.append("vals");
                iterator.previous();
                iterator.remove();
            }
            // Заумно идём к постфиксным параметрам
            iterator.next();
            iterator.next();
            // ... и удаляем их.
            iterator.remove();
            iterator.previous();

            // Складываем аргументы в один массив
            Argument[] args = Arrays.copyOf(prev_val, next_val.length + prev_val.length);
            System.arraycopy(next_val, 0, args, prev_val.length, next_val.length);

            // ... и вызываем нашу функцию.
            ArgumentNode calculated = new ArgumentNode();
            calculated.setArgument(((FunctionNode) node).calculate(args));

            // Ставим вместо функции результат.
            iterator.set(calculated);

//            print(nodes, "", print);
//            System.out.println(print);
//            System.out.println("============:");
        }
    }

    private Argument calculate(List<Node> nodes) {

        ListIterator<Node> iterator;


        // Разкладываем функции по приоритетам
        ArrayList<FunctionNode>
                functions = new ArrayList<>(),
                prefix = new ArrayList<>();

        for (int i = 0; i < nodes.size(); i++)
            if (nodes.get(i) instanceof FunctionNode) {
                if (i - 1 < 0 || nodes.get(i - 1) instanceof FunctionNode)
                    prefix.add((FunctionNode) nodes.get(i));
                else
                    functions.add((FunctionNode) nodes.get(i));
            }


        Collections.sort(functions);
        Collections.sort(prefix);
        prefix.addAll(functions);

        // Решаем функции.
        for (FunctionNode index : prefix) {
            calculateFunction(nodes, nodes.indexOf(index));
        }


        iterator = nodes.listIterator();
        while (iterator.hasNext()) {
            Node node = iterator.next();

            if (node instanceof BracketsNode) {
                Argument value = calculate(((BracketsNode) node).nodes);
                ArgumentNode tmp = new ArgumentNode();
                tmp.setArgument(value);
                iterator.set(tmp);
            }

        }


        if (nodes.size() == 1) return ((ArgumentNode) nodes.get(0)).getArgument();

        double sum = 0;
        for (Node node : nodes) {
            if (node instanceof ArgumentNode)
                sum += (double) ((ArgumentNode) node).getArgument().getValue();
        }

        return new NumberArgument(sum);
    }

}

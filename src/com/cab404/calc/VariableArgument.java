package com.cab404.calc;

/**
 * @author cab404
 */
class VariableArgument extends Argument {
    Object value;
    String name;


    public VariableArgument(String value) {
        super(value);
        name = value.substring(1);
    }

    @Override public Type getType() {
        if (value == null) throw new NullPointerException();

        for (Type type : Type.values())
            if (type.underlying.isInstance(value)) return type;

        throw new RuntimeException("Не могу найти тип переменной!");
    }

    public String getName() {
        return name;
    }

    @Override public void setValue(Object value) {
        this.value = value;
    }

    @Override public Object getValue() {
        return value;
    }

    @Override public boolean isConstant() {
        return false;
    }

    @Override public boolean equals(Object obj) {
        return (obj instanceof VariableArgument) && name.equals(((VariableArgument) obj).getName());
    }

    @Override public int hashCode() {
        return name.hashCode();
    }

    @Override public String toString() {
        return value.toString();
    }
}

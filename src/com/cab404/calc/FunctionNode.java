package com.cab404.calc;

import java.lang.reflect.Constructor;
import java.util.HashSet;
import java.util.Set;

/**
 * @author cab404
 */
class FunctionNode extends Node implements Comparable<FunctionNode> {
    final static Set<Class<? extends FunctionCalculator>> functions;
    private FunctionCalculator calculator;

    public Argument calculate(Argument... values) {
        return calculator.calculate(values);
    }

    static {
        functions = new HashSet<>();
    }
    @Override public int compareTo(FunctionNode functionNode) {
        return calculator.compareTo(functionNode.calculator);
    }


    protected void init() {

        for (Class<? extends FunctionCalculator> function : functions)
            try {
                Constructor<? extends FunctionCalculator> constructor = function.getConstructor();
                FunctionCalculator functionCalculator = constructor.newInstance();

                if (functionCalculator.getExpression().equals(getText())) {
                    calculator = functionCalculator;
                    return;
                }

            } catch (Exception e) {
                e.printStackTrace();
                throw new RuntimeException("Ошибка инициализации: '" + getText() + "'");
            }
        throw new RuntimeException("Неподдерживаемая операция: '" + getText() + "'");

    }

    @Override public String toString() {
        return getText();
    }
}

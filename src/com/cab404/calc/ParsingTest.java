package com.cab404.calc;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.util.List;

import static org.junit.Assert.assertArrayEquals;

/**
 * @author cab404
 */

@RunWith(JUnit4.class)
public class ParsingTest {

    private static Class[] listToClassArray(List<Node> nodes) {
        Class[] out = new Class[nodes.size()];

        int i = 0;
        for (Node node : nodes)
            out[i++] = node.getClass();

        return out;
    }

    private class Exp {
        String exp;
        Class[] expected;
        private Exp(String exp, Class[] expected) {
            this.exp = exp;
            this.expected = expected;
        }
    }

    private static Class
            A = ArgumentNode.class,
            F = FunctionNode.class,
            OB = BracketsNode.class,
            D = DelimeterNode.class;

    Exp[] tests = new Exp[]{
            new Exp("", new Class[]{}),
            new Exp("12", new Class[]{A}),
            new Exp("12 * 2", new Class[]{A, F, A}),
            new Exp("12,12", new Class[]{A, D, A}),
            new Exp("^(42)", new Class[]{F, OB}),
            new Exp("2- -10", new Class[]{A, F, A}),
            new Exp("2 - - 10", new Class[]{A, F, F, A}),
            new Exp("22-(12)+32^(2)", new Class[]{A, F, OB, A, F, OB}),
    };


    @Test
    public void wrapping() {
        Calculation calc = new Calculation();
        int i = 0;
        for (Exp exp : tests) {
            calc.prepareExpression(exp.exp);
            Class[] classes = listToClassArray(calc.nodes);
            assertArrayEquals("Test #" + i++, exp.expected, classes);
        }
    }

}

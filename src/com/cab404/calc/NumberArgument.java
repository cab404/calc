package com.cab404.calc;

/**
 * @author cab404
 */
class NumberArgument extends Argument {

    private double value;

    public NumberArgument(String value) {
        super(value);
        this.value = Double.parseDouble(value);
    }

    public NumberArgument(double value) {
        super(null);
        this.value = value;
    }

    @Override public Type getType() {
        return Type.NUMBER;
    }

    @Override public void setValue(Object value) {
        throw new UnsupportedOperationException();
    }

    @Override public Object getValue() {
        return value;
    }

    @Override public boolean isConstant() {
        return true;
    }

    @Override public String toString() {
        return String.valueOf(value);
    }
}



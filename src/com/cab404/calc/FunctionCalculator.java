package com.cab404.calc;

/**
 * Интерфейс для создания функций калькулятора.
 * Всегда должен быть конструктор без параметров,
 * вызывающий конструктор из этого класса, ибо класс
 * пихается в общюю кучу. Или вызовите register();
 *
 * @author cab404
 * @see BasicCommands
 */
public abstract class FunctionCalculator implements Comparable<FunctionCalculator> {
    /**
     * Кидаем класс в наш массив функций.
     */
    public final void register() {
        FunctionNode.functions.add(getClass());
    }

    public FunctionCalculator() {
        register();
    }

    /**
     * Возвращает название функции, т.е чем её вызывать.
     *
     * @see BasicCommands
     */
    public abstract String getExpression();
    /**
     * Вызывает приоритет функции, чем меньше - тем раньше функция вызывается.
     *
     * @see BasicCommands
     */
    public abstract int getPriority();
    /**
     * Считает функцию по данным аргументам.
     * Аргументы расставляются примерно так:
     * <table>
     * <tr>1 fun 2
     * </tr><tr>1 fun (2,3,4,5,6,7)
     * </tr><tr>42.6 - fun(1,2,3,4,5)
     * </tr>
     * </table>
     */
    public abstract Argument calculate(Argument... values);

    @Override public int compareTo(FunctionCalculator functionCalculator) {
        return getPriority() - functionCalculator.getPriority();
    }
}

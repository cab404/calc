package com.cab404.calc;

import java.util.HashMap;
import java.util.Map;

/**
 * @author cab404
 */
abstract class Node {
    private String text;

    protected abstract void init();

    private static Map<Class<? extends Node>, String> expressions;

    protected Node() {}

    static {
        expressions = new HashMap<>();
        expressions.put(BracketsNode.class, "(\\()");
        expressions.put(CloseBracketNode.class, "(\\))");
        expressions.put(DelimeterNode.class, "(,)");
        expressions.put(ArgumentNode.class, ArgumentNode.getStatementExpression());
        expressions.put(FunctionNode.class, "([A-z/\\+\\-\\*\\=!]+)");
    }

    static String getStatementExpression() {
        StringBuilder ret = new StringBuilder();

        for (String exp : expressions.values())
            ret.append(exp).append("|");

        ret.deleteCharAt(ret.length() - 1);

        return ret.toString();
    }

    public static Node create(String val) {
        Node node = null;

        for (Map.Entry<Class<? extends Node>, String> entry : expressions.entrySet())
            if (val.matches(entry.getValue())) {
                try {
                    node = entry.getKey().newInstance();
                    break;
                } catch (InstantiationException | IllegalAccessException e) {
                    throw new RuntimeException("Неожиданная ошибка при создании нода!", e);
                }
            }
        if (node == null) throw new RuntimeException("Неожиданный оператор : " + val);
        node.text = val;
        node.init();
        return node;
    }
    String getText() {
        return text;
    }
}

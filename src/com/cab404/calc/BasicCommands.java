package com.cab404.calc;

import static com.cab404.calc.Argument.Type;

/**
 * Основной набор команд.
 * Предоставляет команды:
 * <table>
 * <tr>· Сумма (может работать с несколькими параметрами.)
 * </tr><tr>· Разность
 * </tr><tr>· Произведение
 * </tr><tr>· Частное
 * </tr><tr>· Квадратный корень
 * </tr><tr>· Факториал
 * </tr><tr>· Возведение в степень
 * </tr>
 * </table>
 *
 * @author cab404
 */
class BasicCommands {
    /**
     * Добавляет команды в общий список.
     */
    public static void add() {
        new SumFunction();
        new SubFunction();
        new DivFunction();
        new MulFunction();
        new FctFunction();
        new SqrFunction();
        new PowFunction();
        new SetFunction();
        new AssertFunction();
    }

    /**
     * Сумма
     */
    public static class SumFunction extends FunctionCalculator {
        @Override public String getExpression() {
            return "+";
        }
        @Override public int getPriority() {
            return 1024;
        }
        @Override public Argument calculate(Argument... values) {
            double sum = 0;

            for (Argument val : values)
                if (val.getType() == Type.NUMBER)
                    sum += (double) val.getValue();
                else
                    throw new RuntimeException("Нельзя складывать нечисловые значения!");

            return new NumberArgument(sum);
        }
    }

    /**
     * Разность
     */
    public static class SubFunction extends FunctionCalculator {

        @Override public String getExpression() {
            return "-";
        }
        @Override public int getPriority() {
            return 1024;
        }
        @Override public Argument calculate(Argument... values) {
            if (values.length > 1) {

                if (values[0].getType() == Type.NUMBER && values[1].getType() == Type.NUMBER)
                    return new NumberArgument((double) values[0].getValue() - (double) values[1].getValue());
                else
                    throw new RuntimeException("Нельзя вычитать нечисловые значения!");

            } else {

                if (values[0].getType() == Type.NUMBER)
                    return new NumberArgument(-(double) values[0].getValue());
                else
                    throw new RuntimeException("Нельзя вычитать нечисловые значения!");

            }
        }

    }

    /**
     * Произведение
     */
    public static class MulFunction extends FunctionCalculator {
        @Override public String getExpression() {
            return "*";
        }
        @Override public int getPriority() {
            return 512;
        }
        @Override public Argument calculate(Argument... values) {
            if (values[0].getType() == Type.NUMBER && values[1].getType() == Type.NUMBER)
                return new NumberArgument((double) values[0].getValue() * (double) values[1].getValue());
            else throw new RuntimeException("Нельзя перемножать нечисловые аргументы!");
        }
    }

    /**
     * Частное
     */
    public static class DivFunction extends FunctionCalculator {
        @Override public String getExpression() {
            return "/";
        }
        @Override public int getPriority() {
            return 512;
        }
        @Override public Argument calculate(Argument... values) {
            if (values[0].getType() == Type.NUMBER && values[1].getType() == Type.NUMBER)
                return new NumberArgument((double) values[0].getValue() / (double) values[1].getValue());
            else throw new RuntimeException("Нельзя перемножать нечисловые аргументы!");
        }
    }

    /**
     * Факториал
     */
    public static class FctFunction extends FunctionCalculator {
        @Override public String getExpression() {
            return "!";
        }
        @Override public int getPriority() {
            return 256;
        }

        public double factorial(double val, double left) {
            if (left == 0) return val;
            else return factorial(val * left, left - 1);
        }

        @Override public Argument calculate(Argument... values) {
            return new NumberArgument(factorial(1, (double) values[0].getValue()));
        }
    }

    /**
     * Квадратный корень
     */
    public static class SqrFunction extends FunctionCalculator {
        @Override public String getExpression() {
            return "sqrt";
        }
        @Override public int getPriority() {
            return 256;
        }

        @Override public Argument calculate(Argument... values) {
            return new NumberArgument(Math.sqrt((double) values[0].getValue()));
        }
    }

    /**
     * Возведение в степень
     */
    public static class PowFunction extends FunctionCalculator {
        @Override public String getExpression() {
            return "^";
        }
        @Override public int getPriority() {
            return 256;
        }

        @Override public Argument calculate(Argument... values) {
            return new NumberArgument(Math.pow((double) values[0].getValue(), (double) values[1].getValue()));
        }
    }

    /**
     * Функция присвоения
     */
    public static class SetFunction extends FunctionCalculator {

        @Override public String getExpression() {
            return "=";
        }
        @Override public int getPriority() {
            return 4096;
        }
        @Override public Argument calculate(Argument... values) {
            if (values.length == 2) {
                if (!values[0].isConstant()) {
                    values[0].setValue(values[1].getValue());
                    return values[0];
                } else throw new RuntimeException("Невозможно присвоить значение константе!");
            } else throw new RuntimeException("Слишком мало аргументов для присвоения :D");
        }
    }

    /**
     * Функция сранения.
     */
    public static class AssertFunction extends FunctionCalculator {

        @Override public String getExpression() {
            return "==";
        }
        @Override public int getPriority() {
            return 1024;
        }
        @Override public Argument calculate(Argument... values) {
            Argument tmp = null;

            for (Argument arg : values) {
                if (tmp == null || arg.getValue().equals(tmp.getValue()))
                    tmp = arg;
                else return new StringArgument("\"false\"");
            }
            return new StringArgument("\"true\"");
        }
    }

}

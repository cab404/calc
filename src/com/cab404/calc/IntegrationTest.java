package com.cab404.calc;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

/**
 * @author cab404
 */

@RunWith(JUnit4.class)
public class IntegrationTest {

    private static class Exp {
        String exp;
        Object answer;

        private Exp(String exp, Object answer) {
            this.exp = exp;
            this.answer = answer;
        }
    }

    static Exp[] tests = {
            new Exp("1", 1.0),
            new Exp("1+6.9*4-7", 21.6),
            new Exp("22 + 2", 24.0),
            new Exp("22 * 2", 44.0),
            new Exp("783 - 2", 781.0),
            new Exp("12 * (25 + 2)", 324.0),
            new Exp("2^4", 16.0),
            new Exp("2^(4 * 2)", 256.0),
            new Exp("2^(sqrt(16) * 2)", 256.0),
            new Exp("2.5 * 2.5", 6.25),
            new Exp("!3", 6.0),
            new Exp("!(!3)", 720.0),
            new Exp("2- -2", 4.0),
            new Exp("-2 * -3", 6.0),
            new Exp("- sqrt(1)", -1.0),
            new Exp("(2^5) - (6^2) - 4", -8.0),
            new Exp("5 - 5 * 5", -20.0),
            new Exp("!20 / !19", 20.0),
            new Exp("^(42, 2)", 1764.0),
            new Exp("!42", 1.4050061177528801E51),
            new Exp("(4/2)^(2*2)", 16.0),
            new Exp("sqrt 4", 2.0),
            new Exp("\"Test\" == \"Test\"", "true"),
            new Exp("\"Test\" == \"Test\" == \"true\"", "true"),
            new Exp("\"Test\" == \"test\"", "false"),
            new Exp(":var: = 36", 36.0),
            new Exp("sqrt(:var:)", 6.0),
            new Exp(":var: == 36", "true"),
            new Exp(":var: = :var: - :var:", 0.0),
    };

    @Test
    public void integrationTest() {
        Calculation calc = new Calculation();

        int i = 0;
        for (Exp exp : tests) {
            calc.prepareExpression(exp.exp);
            Assert.assertEquals("Test #" + i++, calc.calculate().getValue(), exp.answer);
        }

    }

}

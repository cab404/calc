package com.cab404.calc;

/**
 * @author cab404
 */
public abstract class Argument {

    public enum Type {
        STRING(String.class), NUMBER(Number.class);

        final Class underlying;

        private Type(Class underlying) {
            this.underlying = underlying;
        }
    }

    public Argument(String value) {}

    public abstract void setValue(Object value);
    public abstract Type getType();
    public abstract Object getValue();
    public abstract boolean isConstant();
}

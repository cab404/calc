package com.cab404.calc_poc_ui;

import com.cab404.calc.net.Server;

import java.io.IOException;

/**
 * @author cab404
 */
public class TestServer {

    public static void main(String[] args) {
        try {
            new Thread(new Server(args.length > 0 ? Integer.parseInt(args[0]) : 5544)).start();
            System.out.println("Сервер запущен!");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}

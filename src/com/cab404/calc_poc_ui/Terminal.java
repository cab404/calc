package com.cab404.calc_poc_ui;

import com.cab404.calc.Calculation;

import java.util.Scanner;

/**
 * @author cab404
 */
public class Terminal {
    public static void main(String[] args) {
        Calculation calc = new Calculation();
        Scanner in = new Scanner(System.in);

        while (true) {
            System.out.print(">");
            String exp = in.nextLine();
            if (exp.equals("EXIT")) break;

            exp = exp.replace("-", " - ").replace("+", " + ").replace("*", " * ").replace("/", " / ");

            try {
                calc.prepareExpression(exp);
                System.out.println(calc.calculate());
            } catch (Throwable ex) {
                System.out.println("Ошибка: " + ex.getMessage());
            }
        }
    }
}

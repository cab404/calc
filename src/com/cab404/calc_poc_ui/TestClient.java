package com.cab404.calc_poc_ui;

import com.cab404.calc.net.Client;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * @author cab404
 */
public class TestClient {

    public static void main(String[] args) {
        if (args.length < 2) args = new String[]{"localhost", "5544"};
        try {
            Client client = new Client(args[0], Integer.parseInt(args[1]));
            System.out.println("Подключение установлено!");
            BufferedReader read = new BufferedReader(new InputStreamReader(System.in));
            String in;
            System.out.println();
            System.out.print('>');
            while ((in = read.readLine()) != null) {
                System.out.println(client.calculate(in));
                System.out.print('>');
            }
            client.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
